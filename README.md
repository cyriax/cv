# Jonathan Cyriax Brast

Description of myself
BSc. Computer Science, 

## Timeline
### Education
- 2018-ongoing  Informatics / Autonomous Systems at TU Darmstadt (ongoing)
- 2010-2017     Computer Science/Informatics at Goehte University Frankfurt (BSc. 1.5 "sehr gut")
- 2007-2010     Physics at Goethe University Frankfurt (no degree)
- 1995-2007     Basic Education (Abitur 2.3 "gut")

### Activities
- 2010-2014     System Administration at Goethe University
                server & pool maintainance, custom Linux deployment, helpdesk/support 
- 2011-2017     Student Representative
                councilmember at faculty & institute level, committee activities,
                student parliament/self-governace on university level,
                organization of nationwide student representative conferences
- 2013-2017     Bembelbots RoboCup Soccer SPL
                german/european/world cup participation

## Skills

### Programming Languages
##### `[***** ]` C++ (~10 kSLOC)
> C++ is my language of choice for complex programs and projects,
> where performance is an issue. With the Bembelbots team 
##### `[***** ]` Python (~10 kSLOC)
numpy, pyplot/matplotlib, jupyter notebook, pytorch, PyQt5, OpenCV
> I use Python for Machine Learning, small scripts and toy problems, visualization.
##### `[****  ]` C (~6 kSLOC)
> I have used C in several courses during university:
> (System Programming, High Performance Computing, Parallelization (with MPI))
##### `[***   ]` bash (~5 kSLOC)
> I use bash for all kinds of small scripts and automatization on linux.
> To deploy Linux while working as a sysadmin I also used bash for an extensive custom post install process.
##### `[*     ]` misc (0-2 kSLOC)
haskell, java, go, sed

### Workflows
##### OS/general
Linux, shell, 
##### Development
vim, make, cmake, git
##### Organization
redmine, TODO.txt, taskwarrior

##### 
Linux, zsh, vim, git, make, cmake, redmine, LaTeX, jupyter notebook, 
